<?php
class Close_Button extends Plugin {
	function init($host) {
		$host->add_hook($host::HOOK_ARTICLE_BUTTON, $this);
	}

	function about() {
		return array(null,
			"Adds a button to close article panel",
			"fox");
	}

	function get_css() {
		return ".post .header .buttons i.material-icons.icon-close-article { color : red; }";
	}

	function hook_article_button($line) {
		if (!get_pref(Prefs::COMBINED_DISPLAY_MODE)) {
			return "<i class='material-icons icon-close-article'
				style='cursor : pointer' onclick='Article.close()'
				title=\"".__('Close article')."\">close</i>";
		}
		return "";
	}

	function api_version() {
		return 2;
	}

}
